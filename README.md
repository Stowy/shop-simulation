# Getting started

## Install Beef

### Windows

1. Install [Beef](https://www.beeflang.org/). You can now open the project with the _Beef IDE_.
2. Install the [Microsoft Visual C++ Build Tools](https://visualstudio.microsoft.com/fr/downloads/).

### Linux and MacOS

You have to build _Beef_ from source and the IDE is not available.

## Clone the project

Clone this project with `git clone https://gitlab.com/Stowy/shop-simulation.git`.

You can now open the Beef IDE and click on Open -> Open project... and then click on the BeefProj.toml from the ShopSimulation project.
