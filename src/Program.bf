using System;

namespace ShopSimulation
{
	class Program
	{
		public static void Main()
		{
			let shopApp = scope ShopApp();
			shopApp.Init();
			shopApp.Run();
		}
	}
}
