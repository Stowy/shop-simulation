namespace ShopSimulation
{
	class Entity
	{
		public this()
		{
			Position = .();
		}

		public this(Vector2 pos)
		{
			this.Position = pos;
		}

		public Vector2 Position { get; set mut; }

		public bool IsDeleting { get; set mut; }

		public int32 UpdateCount { get; set mut;}

		public bool IsOffscreen(float marginX, float marginY)
		{
			return ((Position.X < -marginX) || (Position.X >= gShopApp.mWidth + marginX) ||
				(Position.Y < -marginY) || (Position.Y >= gShopApp.mHeight + marginY));
		}

		public virtual void Update()
		{
		}

		public virtual void Draw()
		{
		}
	}
}
