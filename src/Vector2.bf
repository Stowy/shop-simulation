using System;

namespace ShopSimulation
{
	public struct Vector2
	{
		public this()
		{
			this = default;
		}

		public this(float x, float y)
		{
			this.X = x;
			this.Y = y;
		}

		public float X { get; private set mut; }

		public float Y { get; private set mut; }

		// Properties
		public float Magnitude { get => Math.Sqrt((X * X) + (Y * Y)); }

		public float SqrMagnitude { get => (X * X) + (Y * Y); }

		public Vector2 Normalized { get => Normalize(); }

		// Static properties
		public static Vector2 Down { get => .(0, -1); }

		public static Vector2 Left { get => .(-1, 0); }

		public static Vector2 Right { get => .(1, 0); }

		public static Vector2 Up { get => .(0, 1); }

		public static Vector2 NegativeInfinity { get => .(float.NegativeInfinity, float.NegativeInfinity); }

		public static Vector2 PositiveInifinity { get => .(float.PositiveInfinity, float.PositiveInfinity); }

		public static Vector2 One { get => .(1, 1); }

		public static Vector2 Zero { get => .(0, 0); }

		// Methods
		public Vector2 Normalize()
		{
			float magnitude = Magnitude;
			Vector2 t = .();
			t += .(2, 1);
			return .(X / magnitude, Y / magnitude);
		}

		public bool Equals(Vector2 other)
		{
			return X == other.X && Y == other.Y;
		}

		public override void ToString(String strBuffer)
		{
			strBuffer.AppendF("X : {0}, Y : {1}", X, Y);
		}

		public Vector2 DirectionToVector(Vector2 vector){
			return (vector - this).Normalize();
		}

		public float DistanceToVector(Vector2 vector){
			return (vector - this).Magnitude;
		}

		public static Vector2 operator+(Vector2 lVector, Vector2 rVector)
		{
			return .(lVector.X + rVector.X, lVector.Y + rVector.Y);
		}

		public static Vector2 operator-(Vector2 lVector, Vector2 rVector)
		{
			return .(lVector.X - rVector.X, lVector.Y - rVector.Y);
		}

		public static Vector2 operator-(Vector2 val)
		{
			return .(-val.X, -val.Y);
		}

		public static Vector2 operator++(Vector2 val)
		{
			return .(val.X + 1, val.Y + 1);
		}

		public void operator--() mut
		{
			X--;
			Y--;
		}

		public void operator+=(Vector2 val) mut
		{
			X += val.X;
			Y += val.Y;
		}

		public void operator-=(Vector2 val) mut
		{
			X -= val.X;
			Y -= val.Y;
		}

		public static bool operator==(Vector2 lVector, Vector2 rVector)
		{
			float epsilon = 1e-5f;
			return FloatHelper.IsApproximatelyEqual(lVector.X, rVector.X, epsilon)
				&& FloatHelper.IsApproximatelyEqual(lVector.Y, rVector.Y, epsilon);
		}

		public static Vector2 operator*(Vector2 a, float d)
		{
			return .(a.X * d, a.Y * d);
		}

		public static Vector2 operator*(float d, Vector2 a)
		{
			return a * d;
		}
	}
}
