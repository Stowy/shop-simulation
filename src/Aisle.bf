namespace ShopSimulation
{
	public enum Direction
	{
		North,
		South,
		East,
		West
	}

	class Aisle
	{
		public this(Direction direction, int32 distance)
		{
			Direction = direction;
			Distance = distance;
		}

		public Direction Direction { get; set mut; }

		public int32 Distance { get; set mut; }
	}

}
