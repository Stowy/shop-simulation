using SDL2;
using System.Collections;
using System.Diagnostics;

namespace ShopSimulation
{
	class CheckoutCounter : Entity
	{
		public const int32 cWidth = 50;
		public const int32 cHeight = 80;
		public const int32 cMaxCustomers = 5;
		public const float cCheckoutPosOffset = 10f;
		public const int32 cCustomerDeleteRate = 120;

		private bool isOpen;
		private List<Customer> customers ~ delete _;

		public this(bool isOpen, List<Customer> customers)
		{
			IsOpen = isOpen;
			this.customers = customers;
		}

		public this(bool isOpen)
			: this(isOpen, new List<Customer>()) { }

		public bool IsOpen
		{
			get => isOpen;
			set mut => isOpen = value;
		}

		public int OpenSpace
		{
			get
			{
				if (!IsOpen)
				{
					return 0;
				}

				return cMaxCustomers - customers.Count;
			}
		}

		public bool AddCustomer(Customer customer)
		{
			if (OpenSpace <= 0)
			{
				return false;
			}

			customers.Add(customer);
			SetCustomerCheckoutPos(customer);
			return true;
		}

		public int CustomerIndex(Customer customer)
		{
			return customers.IndexOf(customer);
		}

		public void SetCustomerCheckoutPos(Customer customer)
		{
			int customerIndex = CustomerIndex(customer);
			float xCheckoutPos = Position.X + CheckoutCounter.cWidth;
			float yCheckoutPos = Position.Y + CheckoutCounter.cHeight - Customer.cHeight - (customerIndex * (Customer.cHeight + cCheckoutPosOffset));
			customer.CheckoutPos = .(xCheckoutPos, yCheckoutPos);
		}

		public void UpdateCustomersCheckoutPos()
		{
			for (let customer in customers)
			{
				SetCustomerCheckoutPos(customer);
			}
		}

		public override void Draw()
		{
			if (IsOpen)
			{
				SDL.SetRenderDrawColor(gApp.mRenderer, 10, 93, 255, 255);
			}
			else
			{
				SDL.SetRenderDrawColor(gApp.mRenderer, 255, 10, 91, 255);
			}

			SDL.Rect rect = .((int32)Position.X, (int32)Position.Y, cWidth, cHeight);
			SDL.RenderFillRect(gApp.mRenderer, &rect);
		}

		public override void Update()
		{
			if (UpdateCount % cCustomerDeleteRate == 0)
			{
				if (customers.Count > 0)
				{
					customers[0].IsDeleting = true;
					customers.RemoveAt(0);
					UpdateCustomersCheckoutPos();
				}
			}
		}
	}
}
