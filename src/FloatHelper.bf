using System;

namespace ShopSimulation
{
	public static class FloatHelper
	{
		public static bool IsApproximatelyEqual(float a, float b, float epsilon)
		{
			const float floatNormal = (1 << 23) * float.Epsilon;
			float absA = Math.Abs(a);
			float absB = Math.Abs(b);
			float diff = Math.Abs(a - b);

			if (a == b)
			{
				// Shortcut, handles infinities
				return true;
			}

			if (a == 0f || b == 0f || diff < floatNormal)
			{
				// a or b is zero, or both are extremely close to it.
				// relative error is less meaningful here
				return diff < (epsilon * floatNormal);
			}

			// use relative error
			return diff / Math.Min((absA + absB), float.MaxValue) < epsilon;
		}
	}
}
