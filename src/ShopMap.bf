using System.Collections;
using SDL2;

namespace ShopSimulation
{
	class ShopMap : Entity
	{
		// Constants
		public const int32 cDefaultHeight = 10;
		public const int32 cDefaultWidth = 10;

		// Fields
		private List<Aisle> aisles ~ DeleteContainerAndItems!(_);

		// Constructors
		public this(Vector2 pos) : this(pos, new List<Aisle>()) { }

		public this(Vector2 pos, List<Aisle> aisles) : base(pos)
		{
			this.aisles = aisles;
		}

		// Properties
		public Aisle[] Aisles
		{
			get
			{
				Aisle[] array = new Aisle[aisles.Count];
				aisles.CopyTo(array);
				return array;
			}
		}

		// Methods
		public override void Draw()
		{
			int32 x = (int32)Position.X;
			int32 y = (int32)Position.Y;

			// Iterate over every aisles
			for (var aisle in aisles)
			{
				SDL.SetRenderDrawColor(gApp.mRenderer, 165, 165, 165, 255);
				SDL.Rect rect;
				switch (aisle.Direction)
				{
				case Direction.North:
					rect = .(x, y - aisle.Distance + cDefaultHeight, cDefaultWidth, aisle.Distance);
					y -= aisle.Distance;
					break;
				case Direction.East:
					rect = .(x, y, aisle.Distance, cDefaultHeight);
					x += aisle.Distance;
					break;
				case Direction.West:
					rect = .(x - aisle.Distance, y, aisle.Distance + cDefaultWidth, cDefaultHeight);
					x -= aisle.Distance;
				case Direction.South:
					rect = .(x, y, cDefaultWidth, aisle.Distance);
					y += aisle.Distance;
				}

				// Draw the aisle
				SDL.RenderFillRect(gApp.mRenderer, &rect);
			}
		}
	}
}
