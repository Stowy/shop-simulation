using System;
using SDL2;
using System.Diagnostics;

namespace ShopSimulation
{
	class Customer : Entity
	{
		public enum State
		{
			InAisles,
			WaitingForCheckoutCounterLeft,
			WaitingForCheckoutCounterRight,
			AtCheckoutCounter
		}

		
		// // Constants
		public const int32 cMinAge = 0;
		public const int32 cMaxAge = 120;
		public const int32 cWidth = 10;
		public const int32 cHeight = 10;
		public const float cSpeedMultiplier = 2f;

		
		// // Fields
		private int32 age;
		private Aisle[] aisles ~ delete _;
		private Vector2 offset;
		private State currentState;
		private float speed;
		private Vector2 checkoutPos;

		
		// // Constructors
		public this(int32 age, Vector2 pos, Aisle[] map, Vector2 offset, State state)
			: base(pos)
		{
			Age = age;
			Speed = ComputeSpeed();
			aisles = map;
			this.offset = offset;
			CurrentState = state;
			this.CheckoutPos = .();
		}

		public this(int32 age, Vector2 pos, Aisle[] map, Vector2 offset)
			: this(age, pos, map, offset, State.InAisles) { }

		public this(int32 age, Vector2 pos)
			: this(age, pos, new Aisle[](), .Zero) { }

		public this(int32 age)
			: this(age, .()) { }

		
		// // Properties
		public int32 Age
		{
			get => age;
			private set mut
			{
				if (value < cMinAge)
					age = cMinAge;
				else if (value > cMaxAge)
					age = cMaxAge;
				else
					age = value;
			}
		}

		public float Speed
		{
			get => speed;
			private set mut => speed = value;
		}

		public State CurrentState
		{
			get => currentState;
			set mut => currentState = value;
		}

		public Vector2 CheckoutPos
		{
			get => checkoutPos;
			set mut => checkoutPos = value;
		}

		public bool IsWaitingForCheckout
		{
			get => CurrentState == State.WaitingForCheckoutCounterLeft || CurrentState == State.WaitingForCheckoutCounterRight;
		}

		
		// // Methods
		public override void Draw()
		{
			SDL.SetRenderDrawColor(gApp.mRenderer, 255, 255, 255, 255);
			SDL.Rect rect = .((int32)Position.X, (int32)Position.Y, cWidth, cHeight);
			SDL.RenderFillRect(gApp.mRenderer, &rect);
		}

		public override void Update()
		{
			if (!aisles.IsEmpty && CurrentState == State.InAisles)
			{
				FollowPath();
			}
			else if (CurrentState == State.WaitingForCheckoutCounterLeft || CurrentState == State.WaitingForCheckoutCounterRight)
			{
				WaitForCheckoutCounter();
			}
			else if (CurrentState == State.AtCheckoutCounter)
			{
				GoToCheckoutPos();
			}
		}

		public override void ToString(String strBuffer)
		{
			strBuffer.AppendF("Customer, age: {0}, speed: {1}, x: {2}, y: {3}, updateCount: {4}", age, Speed, Position.X, Position.Y, UpdateCount);
		}

		private void GoToCheckoutPos()
		{
			float movementSpeed = speed * 2f;
			Vector2 movement = Position.DirectionToVector(CheckoutPos) * movementSpeed;
			if (Position.DistanceToVector(CheckoutPos) <= movementSpeed)
			{
				Position = CheckoutPos;
			}
			else
			{
				Position += movement;
			}
		}

		private void WaitForCheckoutCounter()
		{
			if (CurrentState == State.WaitingForCheckoutCounterLeft)
			{
				Position = .(Position.X - Speed, Position.Y);
				if (IsOffscreen(0, 0))
				{
					CurrentState = State.WaitingForCheckoutCounterRight;
				}
			}
			else if (CurrentState == State.WaitingForCheckoutCounterRight)
			{
				Position = .(Position.X + Speed, Position.Y);
				if (IsOffscreen(0, 0))
				{
					CurrentState = State.WaitingForCheckoutCounterLeft;
				}
			}
		}

		private float ComputeSpeed()
		{
			float speed = 1;
			if (age >= 18 && age < 25)
			{
				speed = 5;
			}
			else if (age >= 25 && age < 35)
			{
				speed = 4;
			}
			else if (age >= 35 && age < 60)
			{
				speed = 3;
			}
			else if (age >= 60 && age < 80)
			{
				speed = 2;
			}

			return speed * cSpeedMultiplier;
		}

		private void FollowPath()
		{
			int32 traveledDistance = (int32)Math.Round(Speed * UpdateCount);
			int32 xOffset = 0;
			int32 yOffset = 0;
			int32 checkedDistance = 0;
			bool isInAisles = false;

			for (let aisle in aisles)
			{
				if (checkedDistance + aisle.Distance > traveledDistance)
				{
					
					// // The customer is in this aisle
					switch (aisle.Direction)
					{
					case Direction.East:
						xOffset += traveledDistance - checkedDistance;
						break;
					case Direction.South:
						yOffset += traveledDistance - checkedDistance;
						break;
					case Direction.North:
						yOffset -= traveledDistance - checkedDistance;
						break;
					case Direction.West:
						xOffset -= traveledDistance - checkedDistance;
						break;
					}

					isInAisles = true;
					break;
				}
				else
				{
					
					// // The customer is not in this aisle
					checkedDistance += aisle.Distance;
					switch (aisle.Direction)
					{
					case Direction.East:
						xOffset += aisle.Distance;
						break;
					case Direction.South:
						yOffset += aisle.Distance;
						break;
					case Direction.North:
						yOffset -= aisle.Distance;
						break;
					case Direction.West:
						xOffset -= aisle.Distance;
						break;
					}
				}
			}

			if (!isInAisles)
			{
				CurrentState = State.WaitingForCheckoutCounterLeft;
			}

			
			// // Set the customer's position, it is offseted so it can allign with the shop map
			Position = .(xOffset + offset.X, yOffset + offset.Y);
		}
	}
}
