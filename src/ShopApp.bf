using System;
using SDL2;
using System.Collections;

namespace ShopSimulation
{
	static
	{
		public static ShopApp gShopApp;
	}

	class ShopApp : SDLApp
	{
		public const String cWindowTitle = "Shop Simulation";

		// This syntax means that the DeleteContainerAndItems method will be called on the destruction of the instance
		// of this class. The _ represent the current field.
		private List<Entity> entities ~ DeleteContainerAndItems!(_);
		private Font font ~ delete _;
		private Shop shop;

		public this()
		{
			gShopApp = this;
			entities = new List<Entity>();
			font = new Font();
			shop = new Shop();
		}

		public new void Init()
		{
			base.Init();

			font.Load("fonts/dimitri.ttf", 20);
			SDL.SetWindowTitle(this.mWindow, cWindowTitle);

			entities.Add(shop);
		}

		public void DrawString(float x, float y, String str, SDL.Color color, bool centerX = false)
		{
			var x;

			SDL.SetRenderDrawColor(mRenderer, 255, 255, 255, 255);
			let surface = SDLTTF.RenderUTF8_Blended(font.mFont, str, color);
			let texture = SDL.CreateTextureFromSurface(mRenderer, surface);
			SDL.Rect srcRect = .(0, 0, surface.w, surface.h);

			if (centerX)
				x -= surface.w / 2;

			SDL.Rect destRect = .((int32)x, (int32)y, surface.w, surface.h);
			SDL.RenderCopy(mRenderer, texture, &srcRect, &destRect);
			SDL.FreeSurface(surface);
			SDL.DestroyTexture(texture);
		}

		public override void Draw()
		{
			for (var entity in entities)
			{
				entity.Draw();
			}

			DrawString(8, 4, scope String()..AppendF("Clients : {0} / {1}", shop.CustomersCount, Shop.cMaxCustomers), .(255, 255, 255, 255));
			DrawString(8, 24, scope String()..AppendF("Caisses ouvertes : {0} / {1}", shop.OpenCheckoutCountersCount, shop.CheckoutCountersCount), .(255, 255, 255, 255));
			DrawString(8, 44, scope String()..AppendF("Clients qui attendent une caisse : {0}", shop.CustomersWaitingForCheckout), .(255, 255, 255, 255));
			/*DrawString(8, 44, scope String()..AppendF("Temps avant ouverture : 30s"), .(255, 255, 255, 255));
			DrawString(8, 84, scope String()..AppendF("Places disponibles : 5"), .(255, 255, 255, 255));*/
		}

		public void AddEntity(Entity entity)
		{
			entities.Add(entity);
		}

		public override void Update()
		{
			for (var entity in entities)
			{
				entity.UpdateCount++;
				entity.Update();

				// Delete the entities that need to be deleted
				if (entity.IsDeleting)
				{
					if (entity is Customer)
					{
						shop.RemoveCustomer(entity as Customer);
					}
					// '@entity' refers to the enumerator itself
					@entity.Remove();
					delete entity;
				}
			}
		}
	}
}
