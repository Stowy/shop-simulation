using System;
using System.Collections;
using System.Diagnostics;

namespace ShopSimulation
{
	class Shop : Entity
	{
		public const int32 cCustomerSpawnRate = 30;
		public const int32 cMaxCustomers = 30;


		private ShopMap shopMap;
		private List<Customer> customers ~ delete _;
		private Random rnd ~ delete _;
		private List<CheckoutCounter> counters ~ delete _;

		public this()
		{
			// Create the shop map
			let aisles = new List<Aisle>();
			aisles.Add(new Aisle(Direction.East, 970));
			aisles.Add(new Aisle(Direction.South, 50));
			aisles.Add(new Aisle(Direction.West, 550));
			aisles.Add(new Aisle(Direction.South, 30));
			aisles.Add(new Aisle(Direction.West, 420));
			aisles.Add(new Aisle(Direction.South, 100));
			aisles.Add(new Aisle(Direction.East, 600));
			aisles.Add(new Aisle(Direction.North, 30));
			aisles.Add(new Aisle(Direction.West, 430));
			aisles.Add(new Aisle(Direction.North, 30));
			aisles.Add(new Aisle(Direction.East, 700));
			aisles.Add(new Aisle(Direction.South, 150));
			shopMap = new ShopMap(.(20, 130), aisles);
			gShopApp.AddEntity(shopMap);
			customers = new List<Customer>();
			rnd = new Random();

			// Create the checkout counters
			counters = new List<CheckoutCounter>();
			CheckoutCounter counter1 = new CheckoutCounter(false)
				{
					Position = .(50, 500)
				};
			CheckoutCounter counter2 = new CheckoutCounter(false)
				{
					Position = .(130, 500)
				};
			CheckoutCounter counter3 = new CheckoutCounter(false)
				{
					Position = .(210, 500)
				};
			CheckoutCounter counter4 = new CheckoutCounter(false)
				{
					Position = .(290, 500)
				};
			CheckoutCounter counter5 = new CheckoutCounter(false)
				{
					Position = .(370, 500)
				};
			gShopApp.AddEntity(counter1);
			gShopApp.AddEntity(counter2);
			gShopApp.AddEntity(counter3);
			gShopApp.AddEntity(counter4);
			gShopApp.AddEntity(counter5);
			counters.Add(counter1);
			counters.Add(counter2);
			counters.Add(counter3);
			counters.Add(counter4);
			counters.Add(counter5);
		}

		public int CustomersCount { get => customers.Count; }

		public int OpenCheckoutCountersCount
		{
			get
			{
				int count = 0;
				for (let counter in counters)
				{
					if (counter.IsOpen)
						count++;
				}

				return count;
			}
		}

		public int CustomersWaitingForCheckout
		{
			get
			{
				int count = 0;
				for (let customer in customers)
				{
					if (customer.CurrentState != .AtCheckoutCounter && customer.CurrentState != .InAisles)
					{
						count++;
					}
				}

				return count;
			}
		}

		public int CheckoutCountersCount { get => counters.Count; }

		public int SpaceAtCheckout
		{
			get
			{
				int count = 0;
				for (let counter in counters)
				{
					count += counter.OpenSpace;
				}

				return count;
			}
		}

		public void RemoveCustomer(Customer customer)
		{
			if (customers.Contains(customer))
			{
				customers.Remove(customer);
			}
		}

		public override void Update()
		{
			HandleCustomerGeneration();
			HandleCheckoutCounterOpening();
			HandleCustomersGoingToCheckout();
		}

		public void AddCustomer()
		{
			Aisle[] map = shopMap.Aisles;
			Customer ctm = new Customer(rnd.Next(120), .(10, 10), map, shopMap.Position);
			customers.Add(ctm);
			gShopApp.AddEntity(ctm);
		}

		private void HandleCheckoutCounterOpening()
		{
			if (CustomersWaitingForCheckout > CheckoutCounter.cMaxCustomers)
			{
				OpenOneCounter();
			}
			else
			{
				CloseOneCounter();
			}
		}

		private void HandleCustomerGeneration()
		{
			if (UpdateCount % cCustomerSpawnRate == 0 && CustomersCount < cMaxCustomers)
			{
				AddCustomer();
			}
		}

		private void OpenOneCounter()
		{
			for (let counter in counters)
			{
				if (!counter.IsOpen)
				{
					counter.IsOpen = true;
					break;
				}
			}
		}

		private void CloseOneCounter()
		{
			for (let counter in counters)
			{
				if (counter.IsOpen)
				{
					counter.IsOpen = false;
					break;
				}
			}
		}

		private void HandleCustomersGoingToCheckout()
		{
			for (let customer in customers)
			{
				if (customer.IsWaitingForCheckout)
				{
					for (let counter in counters)
					{
						if (counter.OpenSpace > 0)
						{
							counter.AddCustomer(customer);
							customer.CurrentState = Customer.State.AtCheckoutCounter;
							break;
						}
					}
				}
			}
		}
	}
}
