# Logbook

## 25/10/2020

J'ai fini la création des caisses.

## 18/10/2020

J'ai continué la création des caisses.

## 17/10/2020

Faire en sorte que les clients attendent une caisse.

Début de la création des caisses.

Pour stocker le plan du magasin, je ne vais finalement pas utiliser de la sérialisation mais juste un document texte custom.

## 12/10/2020

Modification du code pour qu'il utilise la classe Vector2 et des Get Set.

Modification de customer pour que ce soit lui qui calcule sa position.

## 07/10/2020

Mise en place de submodules pour les dépendences du projet (JSON_Beef et BeefExtensionsLib pour la sérialisation).

Création de la classe Vector2 dans l'optique de remplacer mes x et y par des vecteurs.

## 06/10/2020

Affichage des clients dans le magasin.

## 05/10/2020

J'ai choisi de faire mon projet dans le language _Beef_ qui est un mélange entre le C# et le C++ (pas de GC). Pour l'affichage je vais utiliser SDL2 comme il y a déjà un exemple l'utilisant dans l'éditeur.

J'ai commencé par afficher du texte dans un programme simple pour voir comment ça se passe. Je me suis basé sur le template du jeu de shoot de Beef.

J'ai ensuite réflechis à l'architecture pour faire une file. Je me suis basé sur ce qu'à fait Florian.
